/*
 * userfunctions.h
 *
 *  Created on: Sep 29, 2014
 *      Author: Thomas
 */

#ifndef USERFUNCTIONS_H_
#define USERFUNCTIONS_H_

/*
 * userfunctions.h
 *
 *  Created on: Sep 8, 2014
 *      Author: Thomas
 */

#define LEFT_BACK 1
#define LEFT_MIDDLE 2
#define LEFT_FRONT  3
#define LEFT_MOVER 4
#define RIGHT_BACK 7
#define RIGHT_MIDDLE 8
#define RIGHT_FRONT 9
#define RIGHT_MOVER 10
#define LIFT_PIN 1
#define SOLENOID_PIN_LEFT 2 // B is default
#define SOLENOID_PIN_RIGHT 3

/*
 *
 *
 * USER DEFINED FUNCTIONS
 * - Thomas Culp
 *
 *
 */

void setRightDrive(int speed) {
	if (abs(speed) < 10) {
		motorStop(RIGHT_BACK);
		motorStop(RIGHT_MIDDLE);
		motorStop(RIGHT_FRONT);
	} else {
		motorSet(RIGHT_BACK, speed);
		motorSet(RIGHT_MIDDLE, -1 * speed);
		motorSet(RIGHT_FRONT, speed);
	}
}

void setLeftDrive(int speed) {
	if (abs(speed) < 10) {
		motorStop(LEFT_BACK);
		motorStop(LEFT_MIDDLE);
		motorStop(LEFT_FRONT);
	} else {
		motorSet(LEFT_BACK, speed);
		motorSet(LEFT_MIDDLE, -1 * speed);
		motorSet(LEFT_FRONT, speed);
	}

}

void setLeftMoverWheel(int speed) {
	if (abs(speed) < 10)
		motorStop(LEFT_MOVER);
	else
		motorSet(LEFT_MOVER, speed);
}

void setRightMoverWheel(int speed) {
	if (abs(speed) < 10)
		motorStop(RIGHT_MOVER);
	else
		motorSet(RIGHT_MOVER, speed);
}

void stopMoverWheels() {
	motorStop(RIGHT_MOVER);
	motorStop(LEFT_MOVER);
}

void setLift(bool upOrDown) {
	// let motors go one way or another
	if (upOrDown) {

	} else {

	}
}

void stopLift() {
	motorStop(RIGHT_BACK);
	motorStop(RIGHT_MIDDLE);
	motorStop(RIGHT_FRONT);
	motorStop(LEFT_BACK);
	motorStop(LEFT_MIDDLE);
	motorStop(LEFT_FRONT);
}


/*
 *
 * URGH. Check with Anton on how he wants to execute lifting from the controller...
 * Include that we need to think about intakes/claws, as well.
 *
 */
int getLiftUp() {
	return joystickGetDigital(1, 6, 'u');
}

int getLiftDown() {
	return joystickGetDigital(1, 6, 'd');
}

int getRightJoystick() {
	return joystickGetAnalog(1, 2);
}

int getLeftJoystick() {
	return joystickGetAnalog(1, 3);
}

int getUpperBumperPressed() {
	return joystickGetDigital(1, 6, 'u');
}

int getLowerBumperPressed() {
	return joystickGetDigital(1, 6, 'd');
}

bool getLiftSeated() {
	return digitalRead(LIFT_PIN);
}

int getShiftIntoDrive() {
	return joystickGetDigital(1, 5, 'u');
}

int getShiftIntoLift() {
	return joystickGetDigital(1, 5, 'd');
}

void shiftTransmission(bool driveable) {
	/*
	 * FEED ME. This requires pneumatics stuffs.
	 */
	if (driveable) {
		if (LOW == driveable) {
			setRightDrive(127);
			setRightDrive(0);
		} else if (LOW != driveable) {
			setLeftDrive(127);
			setLeftDrive(0);
		}
		digitalWrite(SOLENOID_PIN_LEFT, LOW);
		digitalWrite(SOLENOID_PIN_RIGHT, LOW);
	} else {
		digitalWrite(SOLENOID_PIN_LEFT, HIGH);
		digitalWrite(SOLENOID_PIN_RIGHT, HIGH);
	}

}




/*
 *
 *
 * INTAKE FUNCTIONS MUST BE COMPLETELY REWRITTEN!
 *
 *
 *
 */
void intake(bool inOrOut) {
	if (inOrOut && motorGet(4) != 127)
		motorSet(4, 127);
	else if (!inOrOut && motorGet(4) != -127)
		motorSet(4, -127);
}

void stopIntake() {
	if (motorGet(4) != 0)
		motorStop(4);
}




#endif /* USERFUNCTIONS_H_ */
