/*
 * userfunctions.h
 *
 *  Created on: Sep 8, 2014
 *      Author: Thomas
 */


#ifndef USERFUNCTIONS_H_
#define USERFUNCTIONS_H_

#define LEFT_BACK 4
#define LEFT_MIDDLE 3
#define LEFT_FRONT 2
#define LEFT_MOVER 1
#define RIGHT_BACK 7
#define RIGHT_MIDDLE 8
#define RIGHT_FRONT 9
#define RIGHT_MOVER 10
#define LIFT_PIN 1
#define SOLENOID_PIN_LEFT 11 /* B is default */
#define SOLENOID_PIN_RIGHT 12

/*
 *
 *
 * USER DEFINED FUNCTIONS
 * - Thomas Culp
 *
 *
 */

void setRightDrive(int velocity) {
	if (abs(velocity) < 10) {
		motorStop(RIGHT_BACK);
		motorStop(RIGHT_MIDDLE);
		motorStop(RIGHT_FRONT);
	} else {
		motorSet(RIGHT_BACK, velocity);
		motorSet(RIGHT_MIDDLE, -1 * velocity);
		motorSet(RIGHT_FRONT, velocity);
	}
}

void setLeftDrive(int velocity) {
	if (abs(velocity) < 10) {
		motorStop(LEFT_BACK);
		motorStop(LEFT_MIDDLE);
		motorStop(LEFT_FRONT);
	} else {
		motorSet(LEFT_BACK, velocity);
		motorSet(LEFT_MIDDLE, -1 * velocity);
		motorSet(LEFT_FRONT, velocity);
	}

}

void setLeftMoverWheel(int velocity) {
	if (abs(velocity) < 10)
		motorStop(LEFT_MOVER);
	else
		motorSet(LEFT_MOVER, velocity);
}

void setRightMoverWheel(int velocity) {
	if (abs(velocity) < 10)
		motorStop(RIGHT_MOVER);
	else
		motorSet(RIGHT_MOVER, velocity);
}

void stopMoverWheels() {
	motorStop(RIGHT_MOVER);
	motorStop(LEFT_MOVER);
}

void setLift(bool upOrDown) {
	/* let motors go one way or another */
	int velocity = upOrDown ? 127 : -127;
	motorSet(RIGHT_BACK, velocity);
	motorSet(RIGHT_MIDDLE, -1 * velocity);
	motorSet(RIGHT_FRONT, velocity);
	motorSet(LEFT_BACK, velocity);
	motorSet(LEFT_MIDDLE, -1 * velocity);
	motorSet(LEFT_FRONT, velocity);
}

void stopLift() {
	/* stopping each motor that isn't a mover */
	int i;
	for (i = 2; i < 5; ++i)
		motorStop(i);
	for (i = 7; i < 10; ++i)
		motorStop(i);
}

int getLiftUp() {
	return joystickGetDigital(1, 6, JOY_UP);
}

int getLiftDown() {
	return joystickGetDigital(1, 6, JOY_DOWN);
}

int getRightJoystick() {
	return joystickGetAnalog(1, 2);
}

int getLeftJoystick() {
	return joystickGetAnalog(1, 3);
}

bool getLiftSeated() {
	return digitalRead(LIFT_PIN);
}

int getShiftIntoDrive() {
	return joystickGetDigital(1, 5, JOY_UP);
}

int getShiftIntoLift() {
	return joystickGetDigital(1, 5, JOY_DOWN);
}

void shiftTransmission(bool lifting) {
	/* LOW is false
	 * HIGH is true and HIGH is in the lifting state
	 */
	digitalWrite(SOLENOID_PIN_LEFT, lifting);
	digitalWrite(SOLENOID_PIN_RIGHT, lifting);
}

/*
 *
 *
 * INTAKE FUNCTIONS MUST BE COMPLETELY REWRITTEN!
 *
 *
 *
 */
void intake(bool inOrOut) {
	if (inOrOut && motorGet(4) != 127)
		motorSet(4, 127);
	else if (!inOrOut && motorGet(4) != -127)
		motorSet(4, -127);
}

void stopIntake() {
	if (motorGet(4) != 0)
		motorStop(4);
}


#endif /* USERFUNCTIONS_H_ */
