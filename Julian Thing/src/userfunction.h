/*
 * userfunction.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Thomas
 */

#ifndef USERFUNCTION_H_
#define USERFUNCTION_H_

/*
 *
 *
 * USER DEFINED FUNCTIONS
 * - Thomas Culp
 *
 *
 */

void rightDrive(int speed) {
	motorSet(10, speed);
}

void leftDrive(int speed) {
	motorSet(1, speed);
}

int getRightJoystick() {
	return joystickGetAnalog(1, 2);
}

int getLeftJoystick() {
	return joystickGetAnalog(1, 3);
}

int getUpperBumperPressed() {
	return joystickGetDigital(1, 6, 'u');
}

int getLowerBumperPressed() {
	return joystickGetDigital(1, 6, 'd');
}

void intake(short inOrOut) {
	if (inOrOut && motorGet(4) != 127)
		motorSet(4, 127);
	else if (!inOrOut && motorGet(4) != -127)
		motorSet(4, -127);
}

void stopIntake() {
	if (motorGet(4) != 0)
		motorStop(4);
}

#endif /* USERFUNCTION_H_ */
