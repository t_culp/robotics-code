/*
 * userfunctions.h
 *
 *  Created on: May 6, 2014
 *      Author: Thomas
 */

#ifndef USERFUNCTIONS_H_
#define USERFUNCTIONS_H_

/*
 *
 *
 * USER DEFINED FUNCTIONS
 * - Thomas Culp
 *
 *
 */

void rightDrive(int speed) {
	motorSet(9, speed);
	motorSet(10, speed);
}

void leftDrive(int speed) {
	motorSet(1, speed);
	motorSet(2, speed);
}

int getRightJoystick() {
	return joystickGetAnalog(1, 2);
}

int getLeftJoystick() {
	return joystickGetAnalog(1, 3);
}

int getUpperBumperPressed() {
	return joystickGetDigital(1, 6, 'u');
}

int getLowerBumperPressed() {
	return joystickGetDigital(1, 6, 'd');
}

void intake(bool inOrOut) {
	if (inOrOut && motorGet(4) != 127)
		motorSet(4, 127);
	else if (!inOrOut && motorGet(4) != -127)
		motorSet(4, -127);
}

void stopIntake() {
	if (motorGet(4) != 0)
		motorStop(4);
}

#endif /* USERFUNCTIONS_H_ */
