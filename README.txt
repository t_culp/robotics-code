FOLLOW VEXY PROGRAMMING AT:
https://bitbucket.org/pHonethechemist/robotics-code

This is the code for Masuk High School's VEX Robotics program, specifically the team So
Vexy It Hurts (4478A). All of the code is written for PROS.

I also could not more possibly recommend switching to PROS if you are currently using 
RobotC: 
http://sourceforge.net/projects/purdueros/

If you need any further convincing, do it here:
http://www.vexforum.com/showthread.php?t=76389

This code is designed for a robot with a transmission; there is a gear set which moves between
powering the drive and powering the lift. Six motors, three on each side, switch from powering 
the back wheels and powering the lift. Two motors, one on each side, power "mover" wheels, 
which operate when the power from the back six motors is used for lifting instead of driving. 
These two motors do not operate when the six motors on the transmission are used for driving.
There are pneumatics which shift the transmission between its two states: lifting and driving.
The pistons are instructed to operate at this time (11-10-14) by instructions from the
controller, the operator indicating by a press of a button whether they wish to either drive
or lift. When the lift is actually put into place, a limit switch will serve as a flag to 
shift the transmission from one state to another depending on if the lift is resting on the 
robot or not.
These operations are what the reader can expect to be reflected in the code. Refer to the So
Vexy It Hurts (4478A) engineering notebook and robot for a visual guide.