commit eed8c404b7da64b7cac8fbe326479ec4794c6231
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Mon Nov 10 10:43:44 2014 -0500

    Refactored code. The userfunctions.h code had an extra brace which was not picked up by the IDE but rather the compiler. The error appeared when the file was read into the corresponding .c file and confusion was abundant. I also figured out that 'u' and 'd' are not the correct character constants for the controller arguments... who would'a thunk it.

commit 91c52104765addbbbd743b5e88a6e52643341996
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Oct 15 20:34:41 2014 -0400

    Git sucks sometimes, fixed up the after-effects of merging.

commit 87cda70116cc327950d73d6d32b5e6806e187961
Merge: 60619f7 1353a3d
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Oct 15 20:31:06 2014 -0400

    Merge branch 'master' of bitbucket.org:pHonethechemist/robotics-code
    
    Conflicts:
    	DRivenshmooten/src/opcontrol.c

commit 60619f7c8b8a8c2b9b87c78bfd5eff5fba38997e
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Oct 15 20:18:15 2014 -0400

    Removed an excess decision structure and embedded it within a more central one.

commit 1353a3d740c5e1bd94a539655e34f2c774190b00
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Oct 15 13:15:56 2014 -0400

    Fixed a syntax error that I can see in the code editor in the PROS IDE.

commit 88b812b3b1dc12687f79528360c7705e19402d02
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Tue Oct 14 15:25:21 2014 -0400

    Switched up some logic to make transmission stuff easier to understand.

commit 25e1ab89aeea431533358dc7a7feeb4aa3413921
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Mon Sep 29 23:05:44 2014 -0400

    Hopefully Bitbucket will acknowledge the README now.

commit 43b789279cb721f7fa5527b27ba04c9ce7bef6a8
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Mon Sep 29 19:13:21 2014 -0400

    Writing up some test things for the pneumatics. Unfortunately the robot is in halves today, so this will be tested another time.

commit e7f520356fa63a80d576b592335daab927a7b498
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Sep 24 16:05:57 2014 -0400

    Added a README.

commit 8fd47cda8b66f8edfae35d9aafa02a628d50e8da
Author: Thomas Culp <thomascculp@gmail.com>
Date:   Wed Sep 24 15:48:38 2014 -0400

    Initial upload.
